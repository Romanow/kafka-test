package ru.romanow.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import ru.romanow.kafka.domain.ExchangeObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by romanow on 26.04.17
 */
@Configuration
public class KafkaConfiguration {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    @Bean
    public Map<String, Object> consumerConfig() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "newHello");

        return props;
    }

    @Bean
    public ConsumerFactory<String, ExchangeObject> consumerFactory() {
        DefaultKafkaConsumerFactory<String, ExchangeObject> factory =
                new DefaultKafkaConsumerFactory<>(consumerConfig());
        factory.setKeyDeserializer(new StringDeserializer());
        factory.setValueDeserializer(new JsonDeserializer<>(ExchangeObject.class));
        return factory;
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, ExchangeObject> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, ExchangeObject> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConcurrency(1);
        factory.setConsumerFactory(consumerFactory());
        factory.getContainerProperties().setAckOnError(false);
        return factory;
    }
}

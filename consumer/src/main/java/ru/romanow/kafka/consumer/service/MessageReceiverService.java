package ru.romanow.kafka.consumer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;
import ru.romanow.kafka.domain.ExchangeObject;

/**
 * Created by romanow on 26.04.17
 */
@Service
public class MessageReceiverService {
    private static final Logger logger = LoggerFactory.getLogger(MessageReceiverService.class);

    private static final String TOPIC_NAME = "news";

    @KafkaListener(topicPartitions = @TopicPartition(topic = TOPIC_NAME,
            partitionOffsets = @PartitionOffset(partition = "0", initialOffset = "-20", relativeToCurrent = "true")))
//    @KafkaListener(topics = TOPIC_NAME)
    public void processEvent(ExchangeObject exchangeObject,
            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) Integer partition,
            @Header(KafkaHeaders.RECEIVED_TOPIC) String topic,
            @Header(KafkaHeaders.OFFSET) Integer offset) {
        logger.info("Topic {}, partition {}, offset {}, message {}", topic, partition, offset, exchangeObject);
    }
}

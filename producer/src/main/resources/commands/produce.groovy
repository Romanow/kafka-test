package commands


import org.crsh.cli.Command
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory
import ru.romanow.kafka.producer.service.ProducerService

/**
 * Created by romanow on 26.04.17
 */
class produce
{

    @Usage("Invoke kafka producer")
    @Command
    def main(InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory")
        ProducerService producerService = beanFactory.getBean(ProducerService.class)
        producerService.send()
    }
}

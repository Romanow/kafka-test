package ru.romanow.kafka.producer.service;

import org.springframework.stereotype.Service;
import ru.romanow.kafka.domain.ExchangeObject;

import java.util.UUID;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Created by romanow on 26.04.17
 */
@Service
public class GeneratorService {
    private static Long order = 0L;

    public ExchangeObject generate() {
        order += 1;
        return new ExchangeObject(UUID.randomUUID(), order, randomAlphabetic(10));
    }
}

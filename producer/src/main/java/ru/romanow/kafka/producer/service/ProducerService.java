package ru.romanow.kafka.producer.service;

import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import ru.romanow.kafka.domain.ExchangeObject;

import java.util.stream.IntStream;

/**
 * Created by romanow on 26.04.17
 */
@Service
public class ProducerService {
    private static final Logger logger = LoggerFactory.getLogger(ProducerService.class);
    private static final String topic = "news";

    @Autowired
    private KafkaTemplate<String, ExchangeObject> kafkaTemplate;

    @Autowired
    private GeneratorService generatorService;

    public void send() {
        IntStream.range(1, 100)
                 .forEach(i -> {
                     try {
                         ExchangeObject object = generatorService.generate();
                         SendResult<String, ExchangeObject> result =
                                 kafkaTemplate.send(topic, object).get();
                         RecordMetadata recordMetadata = result.getRecordMetadata();
                         logger.info("topic = {}, partition = {}, offset = {}, data = {}",
                                 recordMetadata.topic(), recordMetadata.partition(), recordMetadata.offset(), object);
                     } catch (Exception exception) {
                         throw new RuntimeException(exception);
                     }

                 });
    }
}

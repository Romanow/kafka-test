package ru.romanow.kafka.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.romanow.kafka.producer.service.ProducerService;

/**
 * Created by romanow on 26.04.17
 */
@SpringBootApplication
public class ProducerApplication
        implements CommandLineRunner {

    @Autowired
    private ProducerService producerService;

    public static void main(String[] args) {
        SpringApplication.run(ProducerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        while (true) {
            producerService.send();
            Thread.sleep(10000);
        }
    }
}
